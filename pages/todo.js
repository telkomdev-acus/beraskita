import 'isomorphic-fetch';
import React from 'react';
import withRedux from 'next-redux-wrapper';

import Todo from '../components/Todo/index';

import initStore from '../store/configureStore';

class Index extends React.Component {

  render() {
    return (
      <div>
        <div>
          <Todo />
        </div>
      </div>
    );
  }
}

export default withRedux(initStore)(Index);
