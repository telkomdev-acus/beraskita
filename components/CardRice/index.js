import React from 'react';
import PropTypes from 'prop-types';
import Card, { CardContent, CardMedia } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';

const styles = {
  card: {
    display: 'flex'
  },
  cover: {
    width: 35,
    height: 90,
    padding: 25,
    margin: '20px 0 15px 15px',
    display: 'flex',
    backgroundSize: 'contain',
  },
  content: {
    flex: '1 0 auto',
    textAlign: 'left',
  },
  value: {
    color: '#000000',
  },
};

class CardRice extends React.Component {
  render() {
    const { stock, consumption, classes } = this.props;
    return (
      <Card className={classes.card}>
        <CardMedia
          className={classes.cover}
          image="/static/np_wheat_1090038_686868.svg"
        />
        <CardContent className={classes.content}>
          <Typography variant="body2" gutterBottom noWrap className={classes.title}>
            Stok Beras Impor Nasional
          </Typography>
          <Typography variant="display1" gutterBottom noWrap className={classes.value}>
            {stock} Kg
          </Typography>
          <Typography variant="body2" gutterBottom noWrap className={classes.title}>
            Konsumsi Beras Impor Nasional
          </Typography>
          <Typography variant="display1" gutterBottom noWrap className={classes.value}>
            {consumption} Kg
          </Typography>
        </CardContent>
      </Card>
    );
  }
}

CardRice.propTypes = {
  classes: PropTypes.object,
  stock: PropTypes.number,
  consumption: PropTypes.number
};

export default withStyles(styles)(CardRice);