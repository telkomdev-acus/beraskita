import { withStyles } from 'material-ui/styles';
import Component from './component';
import styles from './styles';

export default withStyles(styles)(Component);