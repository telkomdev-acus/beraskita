import React from 'react';
// import PropTypes from 'prop-types';
import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';
import {
  ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend
} from 'recharts';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/Menu/MenuItem';

const styles = {
  card: {
    display: 'flex',
    height: 418,
    maxWidth: '98.7%'
  },
  cover: {
    width: 35,
    height: 25,
    padding: 25,
    margin: '20px 0 15px 15px',
    display: 'flex',
    backgroundSize: 'contain',
  },
  content: {
    flex: '1 0 auto',
    textAlign: 'left',
  },
  value: {
    color: '#000000',
  },
  progress: {
    background: 'none',
    height: 35,
  },
  filterCharts: {
    float: 'right',
    display: 'flex',
    marginRight: 15,
  },
  flex: {
    marginLeft: -30,
  },
  title: {
    textAlign: 'center',
  },
};
const data = [
  {name: '04/04/18', stocks: 100, consumptions: 700},
  {name: '04/05/18', stocks: 300, consumptions: 1000},
  {name: '04/05/18', stocks: 500, consumptions: 1600},
  {name: '04/05/18', stocks: 1000, consumptions: 2400},
];

class Todo extends React.Component {
  state = {
    select1: 8994209000010,
    select2: 1,
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { title, classes, stock_type, jenis_beras } = this.props;
    return (
      <Card className={classes.card}>
        <CardContent className={classes.content}>
          <div className={classes.filterCharts}>
            <TextField
              id="select-currency"
              select
              className={classes.textField}
              value={this.state.select2}
              onChange={this.handleChange('select2')}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              margin="normal"
            >
              {stock_type.map(option => (
                <MenuItem key={option.id_stock_type} value={option.id_stock_type}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </div>
          <div className={classes.filterCharts}>
            <TextField
              id="select-currency"
              select
              className={classes.textField}
              value={this.state.select1}
              onChange={this.handleChange('select1')}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              margin="normal"
            >
              {jenis_beras.map(option => (
                <MenuItem key={option.id_product} value={option.id_product}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </div>
          <Typography variant="body2" gutterBottom className={classes.title}>
            {title}
          </Typography>
          <br/>
          <ResponsiveContainer height={300} className={classes.flex}>
            <LineChart width={600} height={250} data={data}
              margin={{top: 5, right: 50, left: 20, bottom: 5}}>
              <CartesianGrid vertical={false}/>
              <XAxis dataKey="name"/>
              <YAxis/>
              <Tooltip />
              <Legend iconType="square"/>
              <Line type="monotone" dataKey="stocks" stroke="#53d4bc" r={0}/>
              <Line type="monotone" dataKey="consumptions" stroke="#4aa191" r={0}/>
            </LineChart>
          </ResponsiveContainer>
        </CardContent>
      </Card>
    );
  }
}


export default withStyles(styles)(Todo);