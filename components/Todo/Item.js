import React from 'react';
import PropTypes from 'prop-types';

export default class Component extends React.Component {
  render() {
    const { todo, remove } = this.props;

    return (
      <li style={{ listStyle: 'none' }}>
        <button
          className="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-button--colored mdl-js-ripple-effect"
          onClick={() => remove(todo)}
          style={{ fontSize: 12 }}
        >
          x
        </button>{' '}
        {todo.text}
      </li>
    );
  }
}

Component.propTypes = {
  todo: PropTypes.object,
  remove: PropTypes.func
};