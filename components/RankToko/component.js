import React from 'react';
import PropTypes from 'prop-types';
import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import MenuItem from 'material-ui/Menu/MenuItem';

// const dataSelect1 = [
//   {
//     value: 'total-jenis-semua-beras',
//     label: 'Total Semua Jenis Beras',
//   },
//   {
//     value: 'beras-merah',
//     label: 'Beras Merah',
//   },
// ];

// const dataSelect2 = [
//   {
//     value: 'stock',
//     label: 'Stock',
//   },
//   {
//     value: 'beras-merah',
//     label: 'Beras Merah',
//   },
// ];
// const dataSelect3 = [
//   {
//     value: 'tertinggi',
//     label: 'Tertinggi',
//   },
//   {
//     value: 'terbanyak',
//     label: 'Terbanyak',
//   },
// ];

let id = 0;
function createData(namaPemilik, namaToko, alamat, jumlah) {
  id += 1;
  return { id, namaPemilik, namaToko, alamat, jumlah };
}

export default class Component extends React.Component {
  state = {
    select1: 8994209000010,
    select2: 1,
    select3: 'max',
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  _renderTable() {
    const { data, type, classes } = this.props;

    console.log('asdasdsad', data);
    if (type == 'toko') {
      return (
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell className={classes.tableColumn}>Rank</TableCell>
              <TableCell className={classes.tableColumn}>Nama Pemilik</TableCell>
              <TableCell className={classes.tableColumn}>Nama Toko</TableCell>
              <TableCell className={classes.tableColumn}>Alamat</TableCell>
              <TableCell className={classes.tableColumn}>Jumlah</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((n, i) => {
              return (
                <TableRow className={classes.tableColumn} key={i}>
                  <TableCell className={classes.tableColumn}>{i + 1}</TableCell>
                  <TableCell className={classes.tableColumn}>{n.name}</TableCell>
                  <TableCell className={classes.tableColumn}>{n.pic_name}</TableCell>
                  <TableCell className={classes.tableColumn}>{n.address}</TableCell>
                  <TableCell className={classes.tableColumn}>
                    <Typography variant="body1">
                      <br />
                      {`Beras masuk ${n.total} Kg`}
                    </Typography>
                    <Typography variant="caption" style={{ width: n.total + '%',backgroundColor: '#53d4bc' }} className={classes.label} />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      )
    } else {
      return (
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell className={classes.tableColumn}>Rank</TableCell>
              <TableCell className={classes.tableColumn}>Provinsi</TableCell>
              <TableCell className={classes.tableColumn}>Kota</TableCell>
              <TableCell className={classes.tableColumn}>Kecamatan</TableCell>
              <TableCell className={classes.tableColumn}>Jumlah</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((n, i) => {
              return (
                <TableRow className={classes.tableColumn} key={i}>
                  <TableCell className={classes.tableColumn}>{i + 1}</TableCell>
                  <TableCell className={classes.tableColumn}>{n.province}</TableCell>
                  <TableCell className={classes.tableColumn}>{n.city}</TableCell>
                  <TableCell className={classes.tableColumn}>{n.district}</TableCell>
                  <TableCell className={classes.tableColumn}>
                    <Typography variant="body1">
                      <br />
                      {`Beras masuk ${n.stock} Kg`}
                    </Typography>
                    <Typography variant="caption" style={{ width: (n.stock) + '%',backgroundColor: '#53d4bc' }} className={classes.label} />
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      )
    }
  }

  render() {
    const { title, classes, order, stock_type, jenis_beras } = this.props;

    return (
      <Card >
        <CardContent>
          <div sytle={{ display: 'inline-block' }}>
            <Typography variant="body2" gutterBottom noWrap className={classes.title}>
              {title}
            </Typography>
          </div>
          <div className={classes.filterCharts}>
            <TextField
              id="select-currency"
              select
              className={classes.textField}
              value={this.state.select3}
              onChange={this.handleChange('select3')}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              margin="normal"
            >
              {order.map(option => (
                <MenuItem key={option.val} value={option.val}>
                  {option.text}
                </MenuItem>
              ))}
            </TextField>
          </div>
          <div className={classes.filterCharts}>
            <TextField
              id="select-currency"
              select
              className={classes.textField}
              value={this.state.select2}
              onChange={this.handleChange('select2')}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              margin="normal"
            >
              {stock_type.map(option => (
                <MenuItem key={option.id_stock_type} value={option.id_stock_type}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </div>
          <div className={classes.filterCharts}>
            <TextField
              id="select-currency"
              select
              className={classes.textField}
              value={this.state.select1}
              onChange={this.handleChange('select1')}
              SelectProps={{
                MenuProps: {
                  className: classes.menu,
                },
              }}
              margin="normal"
            >
              {jenis_beras.map(option => (
                <MenuItem key={option.id_product} value={option.id_product}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </div>
          {this._renderTable()}
        </CardContent>
      </Card>
    );
  }
}

Component.propTypes = {
  title: PropTypes.string,
  classes: PropTypes.object,
  order: PropTypes.array,
  stock_type: PropTypes.array,
  jenis_beras: PropTypes.array,
  data: PropTypes.array,
  type: PropTypes.string
}